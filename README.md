# Gitlab Slacker

## Run Docker

```
docker run \
-e SLACK_WEBHOOK_URL="<SLACK_WEBHOOK_URL>" \
-e GITLAB_TOKEN="<GITLAB_TOKEN>" \
-e GITLAB_PROJECT_IDS="<GITLAB_PROJECT_IDS>" \
-e GITLAB_MR_TARGET_BRANCH="<GITLAB_MR_TARGET_BRANCH>" \
registry.gitlab.com/joemielke/gitlab-slacker:0.0.2
```

_note_: `GITLAB_PROJECT_IDS` is a comma separated list of ids, for example `GITLAB_PROJECT_IDS="12345678,87654321"`

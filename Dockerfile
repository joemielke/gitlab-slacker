FROM node:lts

WORKDIR /app
COPY . .
RUN npm ci
RUN npm run build

ENV GITLAB_TOKEN=
ENV GITLAB_PROJECT_IDS=
ENV SLACK_WEBHOOK_URL=

CMD [ "node", "dist/index.js" ]
import { Gitlab } from '@gitbeaker/node';
import { sendMergeRequestsSlack as sendMergeRequestsSlackForProject } from './merge-requests';

export const gitlab = new Gitlab({
  token: process.env.GITLAB_TOKEN
});

function checkConfig() {
  if (!process.env.SLACK_WEBHOOK_URL) {
    throw Error('SLACK_WEBHOOK_URL must be set');
  }
  if (!process.env.GITLAB_TOKEN) {
    throw Error('GITLAB_TOKEN must be set');
  }
  if (!process.env.GITLAB_PROJECT_IDS) {
    throw Error('GITLAB_PROJECT_IDS must be set');
  }
  if (!process.env.GITLAB_MR_TARGET_BRANCH) {
    process.env.GITLAB_MR_TARGET_BRANCH = 'main';
    // throw Error("GITLAB_MR_TARGET_BRANCH must be set");
  }
}
function sendMergeRequestSlacks() {
  const projectIds: string[] = (process.env.GITLAB_PROJECT_IDS || '').split(
    ','
  );
  const targetBranch: string = process.env.GITLAB_MR_TARGET_BRANCH || 'main';
  projectIds.forEach((projectId) =>
    sendMergeRequestsSlackForProject(Number(projectId), targetBranch)
  );
}

checkConfig();
sendMergeRequestSlacks();

import {
  MergeRequestSchema,
  ProjectExtendedSchema
} from '@gitbeaker/core/dist/types/types';
import { IncomingWebhook } from '@slack/webhook';
import { KnownBlock } from '@slack/types';
import { gitlab } from '.';

interface RepoMergeRequests {
  project: ProjectExtendedSchema;
  mergeRequests: MergeRequestData[];
  targetBranch: string;
}
interface MergeRequestData {
  mrId: number;
  title: string;
  author: string;
  url: string;
  approvers: (string | undefined)[];
  groups: (string | undefined)[];
  reviewers: string[];
  threads: MergeRequestThread[];
  pipelineStatus: string;
}

interface MergeRequestThread {
  open: boolean;
  commentsBy: string[];
}

export const sendMergeRequestsSlack = async (
  projectId: number,
  targetBranch: string
) => {
  const project = await gitlab.Projects.show(projectId);
  const mrs = await getOpenMergeRequestsToBranch(projectId, targetBranch);
  const mrsWithDataPromises = mrs.map((mr) =>
    getMergeRequestData(mr, projectId)
  );
  const results = await Promise.all(mrsWithDataPromises);
  const flattenedResults = results.flat();

  const repoMergeRequests: RepoMergeRequests = {
    project,
    targetBranch,
    mergeRequests: flattenedResults
  };

  // console.log(JSON.stringify(repoMergeRequests))
  sendSlack(repoMergeRequests);
};

async function getOpenMergeRequestsToBranch(
  projectId: number,
  targetBranch: string
) {
  return await gitlab.MergeRequests.all({
    projectId,
    state: 'opened',
    wip: 'no',
    targetBranch,
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    approvedByIds: 'None'
  });
}

async function getMergeRequestData(
  mr: MergeRequestSchema,
  projectId: number
): Promise<MergeRequestData> {
  const fullMR = await gitlab.MergeRequests.show(projectId, mr.iid);
  const rules = await gitlab.MergeRequestApprovals.approvalRules(projectId, {
    mergerequestIid: mr.iid
  });
  const eligibleApprovers = rules.flatMap((r) =>
    r.eligible_approvers?.map((ea) => ea.username)
  );
  const groups = rules.flatMap((r) => r.groups?.map((g) => g.name));
  const discussions = await gitlab.MergeRequestDiscussions.all(
    projectId,
    mr.iid
  );
  const threads: MergeRequestThread[] = discussions
    .filter((t) => t.notes?.some((n) => n.resolvable))
    .map((t) => ({
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      open: t.notes?.some((n) => !n.resolved) || false,
      commentsBy:
        t.notes?.reduce<string[]>((prev, current) => {
          prev.push(String(current.author.name));
          const uniqueList = [...new Set(prev)];
          return uniqueList;
        }, []) || []
    }));

  const reviewers: string[] = mr.reviewers?.map((r) => String(r.name)) || [];

  return {
    mrId: mr.iid,
    title: mr.title,
    author: mr.author.name as string,
    url: mr.web_url,
    approvers: [...new Set(eligibleApprovers)],
    groups: [...new Set(groups)],
    reviewers,
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    pipelineStatus: fullMR.head_pipeline?.status,
    threads
  };
}

function sendSlack(repoMergeRequests: RepoMergeRequests) {
  new IncomingWebhook(process.env.SLACK_WEBHOOK_URL || '').send({
    blocks: [
      {
        type: 'header',
        text: {
          type: 'plain_text',
          text: `These [${repoMergeRequests.project.name}] MRs to [${repoMergeRequests.targetBranch}] need attention`
        }
      },
      ...repoMergeRequests.mergeRequests.flatMap((mr): KnownBlock[] => [
        {
          type: 'context',
          elements: [
            {
              type: 'image',
              image_url:
                mr.pipelineStatus == 'success'
                  ? 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Green_Dot_%28Active%29.png/240px-Green_Dot_%28Active%29.png'
                  : 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Location_dot_dark_red.svg/240px-Location_dot_dark_red.svg.png',
              alt_text: 'success'
            },
            {
              type: 'mrkdwn',
              text: `${mr.mrId} - <${mr.url}|${mr.title}>\n*author*: ${
                mr.author
              }${formatReviewers(mr)}${formatCodeOwners(mr)}${formatOpenThreads(
                mr
              )}${formatCommentsBy(mr)}`
            }
            // {
            //   type: "mrkdwn",
            //   text: `open threads: ${mr.openThreads} `
            // },
          ]
        }
      ])
    ]
  });
}

function formatReviewers(mr: MergeRequestData) {
  return mr.reviewers.length
    ? ` | *reviewers*: ${mr.reviewers.join(', ')}`
    : '';
}

function formatCodeOwners(mr: MergeRequestData) {
  return mr.groups.length ? ` | *code owners*: ${mr.groups.join(', ')}` : '';
}

function formatOpenThreads(mr: MergeRequestData) {
  return mr.threads
    ? ` | *open threads*: ${mr.threads.filter((t) => t.open).length}`
    : '';
}

function formatCommentsBy(mr: MergeRequestData) {
  return mr.threads
    ? ` | *comments by*: ${[
        ...new Set(mr.threads.flatMap((t) => t.commentsBy))
      ].join(', ')}`
    : '';
}
